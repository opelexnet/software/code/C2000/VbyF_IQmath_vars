/*
 * macros.h
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */

#ifndef MACROS_H_
#define MACROS_H_

#define GLOBAL_Q    20    // Q value of fixed point number. Used in IQmathLib.h

// Floating point values. Has to be converted to _IQ format before use.
#define PI          3.141592654
#define FS          10000.0
#define TS          1.0/FS
#define VDC         200.0
#define VD_MIN      10.0
#define F_RATED     50.0

// _IQ constants used in control algorithm
#define K_THETA     _IQ(2*PI*TS)     // 2*pi*Ts
#define K_VBYF      _IQ((VDC*0.5)/F_RATED)  // _IQ(100V/50Hz)
#define IQ_HALF     _IQ(0.5)
#define IQ_SQRT3BY2 _IQ(1.732050807/2.0)
#define F_STEP      _IQ(0.5/FS)

// Constants used for PWM generation
#define RAMP   0

#if RAMP
#define PERIOD 9000   // fs = 10000 Hz, up count (RAMP Carrier)
#else                 // not RAMP, Traingular carrier
#define PERIOD 4500   // fs = 10000 Hz, up-down count
#endif // RAMP

#endif /* MACROS_H_ */
