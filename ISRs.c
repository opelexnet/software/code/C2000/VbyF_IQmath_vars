/*
 * ISRs.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   Interrupt Service Routines
//


#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes
#include "macros.h"            // Include macro definitions
#include "IQmathLib.h"         // Include IQmath functions & macro declarations
#include "GlobalVariables.h"   // Include Global Variable declarations

// Interrupt routines uses in this example:
__interrupt void epwm1_ISR(void)
{
    _iq del;

    // Update freq to reach freq_set. Update is limited by slew rate (F_STEP)
    if (freq < freq_set)
    {
        freq += F_STEP;
    }
    if (freq > freq_set)
    {
        freq -= F_STEP;
    }

    // update theta from freq
    del = _IQmpy(freq,K_THETA);
    theta += del;
    if (theta > _IQ(2*PI))
    {
        theta -= _IQ(2*PI);
    }
    /*   if (theta > _IQ(1.0))
         {
         theta -= _IQ(1.0);
         }*/

    // Read sinTheta and cosTheta from sine table
    sinTheta = _IQsin(theta);
    cosTheta = _IQcos(theta);

    // V by f
    Vd = _IQmpy(_IQabs(freq),K_VBYF);  // Vd gets voltage required for current frequency
    if (Vd < _IQ(VD_MIN))      // Ensure minimum voltage
    {
        Vd = _IQ(VD_MIN);
    }
    // Vq = 0;                    // Vq is not used

    // D,Q to Al,Be
    Valfa = _IQmpy(Vd,cosTheta) /* + _IQmpy(Vq,-sinTheta)*/;
    Vbeta = _IQmpy(Vd,sinTheta) /* + _IQmpy(Vq, cosTheta)*/;

    // Al,Be to a,b,c
    Va = Valfa;
    Vb = _IQmpy(Valfa,-IQ_HALF) + _IQmpy(Vbeta, IQ_SQRT3BY2);
    Vc = _IQmpy(Valfa,-IQ_HALF) + _IQmpy(Vbeta,-IQ_SQRT3BY2);

    // Timing from Voltages
    Ta = _IQmpy(_IQdiv(Va,Vdc),PERIOD);
    Tb = _IQmpy(_IQdiv(Vb,Vdc),PERIOD);
    Tc = _IQmpy(_IQdiv(Vc,Vdc),PERIOD);

    // Find Toffset
    Tmax = (  Ta>Tb) ?   Ta : Tb;
    Tmax = (Tmax>Tc) ? Tmax : Tc;
    Tmin = (  Ta<Tb) ?   Ta : Tb;
    Tmin = (Tmin<Tc) ? Tmin : Tc;
    Teff = Tmax - Tmin;
    Tzero = PERIOD - Teff;
    switch (pwm)
    {
        case SINE   :
            Toffset = PERIOD>>1;
            break;
        case SVPWM  :
            Toffset = (Tzero>>1) - Tmin;
            break;
        case LCLAMP :
            Toffset = -Tmin;
            break;
        case HCLAMP :
            Toffset = PERIOD - Tmax;
            break;
    }

    // Add offset to Timings
    Tao = Ta + Toffset;
    Tbo = Tb + Toffset;
    Tco = Tc + Toffset;

    EPwm1Regs.CMPA.half.CMPA = Tao;
    EPwm2Regs.CMPA.half.CMPA = Tbo;
    EPwm3Regs.CMPA.half.CMPA = Tco;

    // Va = sinTheta + _IQ(1.0);
    // Ta = _IQmpy(Va,PERIOD/2);
    EPwm7Regs.CMPA.half.CMPA = Tao;

    // Vb = cosTheta + _IQ(1.0);
    // Tb = _IQmpy(Vb,PERIOD/2);
    // Ta = _IQdiv(theta,_IQ(2.0*PI));
    // Tb = _IQmpyI32int(Ta,PERIOD);
    EPwm7Regs.CMPB = Tbo;

    // Clear INT flag for this timer
    EPwm1Regs.ETCLR.bit.INT = 1;

    // Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

// Default ISR for unused interrupts
__interrupt void default_ISR(void)
{
    // Trap non initialized interrupts
    while(1);
}

// ISR for reserved interrupts
__interrupt void rsvd_ISR(void)
{
    // Trap reserved interrupts
    __asm ("      ESTOP0");
    while(1);
}

// ISR for reserved space in PIE vector table
__interrupt void PIE_RESERVED(void)
{
    __asm ("      ESTOP0");
    for(;;);
}
