/*
 * GlobalVariables.h
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */

#ifndef GLOBALVARIABLES_H_
#define GLOBALVARIABLES_H_

// Frequency and angles
extern _iq  freq, freq_set;
extern _iq  theta;
extern _iq  sinTheta, cosTheta;

// Voltages
extern _iq  Vdc;
extern _iq  Vd, Vq;
extern _iq  Valfa, Vbeta;
extern _iq  Va, Vb, Vc;

// PWM Timing computation
extern _iq  Ta, Tb, Tc;
extern _iq  Tmax, Tmin;
extern _iq  Teff, Tzero, Toffset;
extern _iq  Tao, Tbo, Tco;

// define enum type for PWM technique
typedef enum {SINE, SVPWM, LCLAMP, HCLAMP} pwm_t;
extern pwm_t pwm;

#endif /* GLOBALVARIABLES_H_ */
