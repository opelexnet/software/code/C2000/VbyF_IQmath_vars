/*
 * Interrupt.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   Interrupts Initialization & Support Functions.
//

#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes

//---------------------------------------------------------------------------
// InitPieVectTable:
//---------------------------------------------------------------------------
// This function initializes the PIE vector table with appropriate ISR addresses.
// This function must be executed after boot time.
//
void InitPieVectTable(void)
{
    // Interrupts that are used in this project are mapped to
    // ISR functions found in ISRs.c file.
    // Map used interrupts to the corrsponding ISRs
    // Map other interrupts to either default_ISR, rsvd_ISR or PIE_RESERVED

    EALLOW;  // This is needed to write to EALLOW protected registers

    PieVectTable.PIE1_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE2_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE3_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE4_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE5_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE6_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE7_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE8_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE9_RESERVED  = &PIE_RESERVED;
    PieVectTable.PIE10_RESERVED = &PIE_RESERVED;
    PieVectTable.PIE11_RESERVED = &PIE_RESERVED;
    PieVectTable.PIE12_RESERVED = &PIE_RESERVED;
    PieVectTable.PIE13_RESERVED = &PIE_RESERVED;

    // Non-Peripheral Interrupts:
    PieVectTable.TINT1   = &default_ISR; // CPU-Timer1
    PieVectTable.TINT2   = &default_ISR; // CPU-Timer2
    PieVectTable.DATALOG = &default_ISR; // Datalogging interrupt
    PieVectTable.RTOSINT = &default_ISR; // RTOS interrupt
    PieVectTable.EMUINT  = &default_ISR; // Emulation interrupt
    PieVectTable.NMI     = &default_ISR; // Non-maskable interrupt
    PieVectTable.ILLEGAL = &default_ISR; // Illegal operation TRAP
    PieVectTable.USER1   = &default_ISR; // User Defined trap 1
    PieVectTable.USER2   = &default_ISR; // User Defined trap 2
    PieVectTable.USER3   = &default_ISR; // User Defined trap 3
    PieVectTable.USER4   = &default_ISR; // User Defined trap 4
    PieVectTable.USER5   = &default_ISR; // User Defined trap 5
    PieVectTable.USER6   = &default_ISR; // User Defined trap 6
    PieVectTable.USER7   = &default_ISR; // User Defined trap 7
    PieVectTable.USER8   = &default_ISR; // User Defined trap 8
    PieVectTable.USER9   = &default_ISR; // User Defined trap 9
    PieVectTable.USER10  = &default_ISR; // User Defined trap 10
    PieVectTable.USER11  = &default_ISR; // User Defined trap 11
    PieVectTable.USER12  = &default_ISR; // User Defined trap 12

    // Group 1 PIE Peripheral Vectors:
    PieVectTable.ADCINT1 = &default_ISR; // ADC - if Group 10 ADCINT1 is enabled, this must be rsvd1_1
    PieVectTable.ADCINT2 = &default_ISR; // ADC - if Group 10 ADCINT2 is enabled, this must be rsvd1_2
    PieVectTable.rsvd1_3 = &rsvd_ISR;
    PieVectTable.XINT1   = &default_ISR; // External Interrupt 1
    PieVectTable.XINT2   = &default_ISR; // External Interrupt 2
    PieVectTable.ADCINT9 = &default_ISR; // ADC 9
    PieVectTable.TINT0   = &default_ISR; // Timer 0
    PieVectTable.WAKEINT = &default_ISR; // WD

    // Group 2 PIE Peripheral Vectors:
    PieVectTable.EPWM1_TZINT = &default_ISR; // EPWM-1
    PieVectTable.EPWM2_TZINT = &default_ISR; // EPWM-2
    PieVectTable.EPWM3_TZINT = &default_ISR; // EPWM-3
    PieVectTable.EPWM4_TZINT = &default_ISR; // EPWM-4
    PieVectTable.EPWM5_TZINT = &default_ISR; // EPWM-5
    PieVectTable.EPWM6_TZINT = &default_ISR; // EPWM-6
    PieVectTable.EPWM7_TZINT = &default_ISR; // EPWM-7
    PieVectTable.EPWM8_TZINT = &default_ISR; // EPWM-8

    // Group 3 PIE Peripheral Vectors:
    PieVectTable.EPWM1_INT = &epwm1_ISR;    // EPWM-1
    PieVectTable.EPWM2_INT = &default_ISR;  // EPWM-2
    PieVectTable.EPWM3_INT = &default_ISR;  // EPWM-3
    PieVectTable.EPWM4_INT = &default_ISR;  // EPWM-4
    PieVectTable.EPWM5_INT = &default_ISR;  // EPWM-5
    PieVectTable.EPWM6_INT = &default_ISR;  // EPWM-6
    PieVectTable.EPWM7_INT = &default_ISR;  // EPWM-7
    PieVectTable.EPWM8_INT = &default_ISR;  // EPWM-8

    // Group 4 PIE Peripheral Vectors:
    PieVectTable.ECAP1_INT  = &default_ISR; // ECAP-1
    PieVectTable.ECAP2_INT  = &default_ISR; // ECAP-2
    PieVectTable.ECAP3_INT  = &default_ISR; // ECAP-3
    PieVectTable.rsvd4_4    = &rsvd_ISR;
    PieVectTable.rsvd4_5    = &rsvd_ISR;
    PieVectTable.rsvd4_6    = &rsvd_ISR;
    PieVectTable.HRCAP1_INT = &default_ISR; // HRCAP-1
    PieVectTable.HRCAP2_INT = &default_ISR; // HRCAP-2

    // Group 5 PIE Peripheral Vectors:
    PieVectTable.EQEP1_INT  = &default_ISR; // EQEP-1
    PieVectTable.EQEP2_INT  = &default_ISR; // EQEP-2
    PieVectTable.rsvd5_3    = &rsvd_ISR;
    PieVectTable.HRCAP3_INT = &default_ISR; // HRCAP-3
    PieVectTable.HRCAP4_INT = &default_ISR; // HRCAP-4
    PieVectTable.rsvd5_6    = &rsvd_ISR;
    PieVectTable.rsvd5_7    = &rsvd_ISR;
    PieVectTable.USB0_INT   = &default_ISR; // USB-0

    // Group 6 PIE Peripheral Vectors:
    PieVectTable.SPIRXINTA = &default_ISR; // SPI-A
    PieVectTable.SPITXINTA = &default_ISR; // SPI-A
    PieVectTable.SPIRXINTB = &default_ISR; // SPI-B
    PieVectTable.SPITXINTB = &default_ISR; // SPI-B
    PieVectTable.MRINTA    = &default_ISR; // McBSP-A
    PieVectTable.MXINTA    = &default_ISR; // McBSP-A
    PieVectTable.rsvd6_7   = &rsvd_ISR;
    PieVectTable.rsvd6_8   = &rsvd_ISR;

    // Group 7 PIE Peripheral Vectors:
    PieVectTable.DINTCH1 = &default_ISR;  // DMA CH1
    PieVectTable.DINTCH2 = &default_ISR;  // DMA CH2
    PieVectTable.DINTCH3 = &default_ISR;  // DMA CH3
    PieVectTable.DINTCH4 = &default_ISR;  // DMA CH4
    PieVectTable.DINTCH5 = &default_ISR;  // DMA CH5
    PieVectTable.DINTCH6 = &default_ISR;  // DMA CH6
    PieVectTable.rsvd7_7 = &rsvd_ISR;
    PieVectTable.rsvd7_8 = &rsvd_ISR;

    // Group 8 PIE Peripheral Vectors:
    PieVectTable.I2CINT1A = &default_ISR;  // I2C-A
    PieVectTable.I2CINT2A = &default_ISR;  // I2C-A
    PieVectTable.rsvd8_3  = &rsvd_ISR;
    PieVectTable.rsvd8_4  = &rsvd_ISR;
    PieVectTable.rsvd8_5  = &rsvd_ISR;
    PieVectTable.rsvd8_6  = &rsvd_ISR;
    PieVectTable.rsvd8_7  = &rsvd_ISR;
    PieVectTable.rsvd8_8  = &rsvd_ISR;

    // Group 9 PIE Peripheral Vectors:
    PieVectTable.SCIRXINTA = &default_ISR;  // SCI-A
    PieVectTable.SCITXINTA = &default_ISR;  // SCI-A
    PieVectTable.SCIRXINTB = &default_ISR;  // SCI-B
    PieVectTable.SCITXINTB = &default_ISR;  // SCI-B
    PieVectTable.ECAN0INTA = &default_ISR;  // eCAN-A
    PieVectTable.ECAN1INTA = &default_ISR;  // eCAN-A
    PieVectTable.rsvd9_7   = &rsvd_ISR;
    PieVectTable.rsvd9_8   = &rsvd_ISR;

    // Group 10 PIE Peripheral Vectors:
    PieVectTable.rsvd10_1 = &rsvd_ISR;  // Can be ADCINT1, but must make ADCINT1 in Group 1 space "reserved".
    PieVectTable.rsvd10_2 = &rsvd_ISR;  // Can be ADCINT2, but must make ADCINT2 in Group 1 space "reserved".
    PieVectTable.ADCINT3  = &default_ISR;  // ADC
    PieVectTable.ADCINT4  = &default_ISR;  // ADC
    PieVectTable.ADCINT5  = &default_ISR;  // ADC
    PieVectTable.ADCINT6  = &default_ISR;  // ADC
    PieVectTable.ADCINT7  = &default_ISR;  // ADC
    PieVectTable.ADCINT8  = &default_ISR;  // ADC

    // Group 11 PIE Peripheral Vectors:
    PieVectTable.CLA1_INT1 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT2 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT3 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT4 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT5 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT6 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT7 = &default_ISR;  // CLA
    PieVectTable.CLA1_INT8 = &default_ISR;  // CLA

    // Group 12 PIE Peripheral Vectors:
    PieVectTable.XINT3    = &default_ISR;
    PieVectTable.rsvd12_2 = &rsvd_ISR;
    PieVectTable.rsvd12_3 = &rsvd_ISR;
    PieVectTable.rsvd12_4 = &rsvd_ISR;
    PieVectTable.rsvd12_5 = &rsvd_ISR;
    PieVectTable.rsvd12_6 = &rsvd_ISR;
    PieVectTable.LVF      = &default_ISR;  // Latched overflow
    PieVectTable.LUF      = &default_ISR;  // Latched underflow

    EDIS;    // This is needed to disable write to EALLOW protected registers

}

void InitInterrupts(void)
{

    // Disable Interrupts at the CPU level:
    DINT;

    // Disable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 0;

    // Clear all PIEIFR registers:
    PieCtrlRegs.PIEIFR1.all  = 0;
    PieCtrlRegs.PIEIFR2.all  = 0;
    PieCtrlRegs.PIEIFR3.all  = 0;
    PieCtrlRegs.PIEIFR4.all  = 0;
    PieCtrlRegs.PIEIFR5.all  = 0;
    PieCtrlRegs.PIEIFR6.all  = 0;
    PieCtrlRegs.PIEIFR7.all  = 0;
    PieCtrlRegs.PIEIFR8.all  = 0;
    PieCtrlRegs.PIEIFR9.all  = 0;
    PieCtrlRegs.PIEIFR10.all = 0;
    PieCtrlRegs.PIEIFR11.all = 0;
    PieCtrlRegs.PIEIFR12.all = 0;

    // Enable Used Interrupts. Clear all other PIEIER registers:
    PieCtrlRegs.PIEIER1.all  = 0;
    PieCtrlRegs.PIEIER2.all  = 0;
    PieCtrlRegs.PIEIER3.all  = 1; //Enable EPWM1 interrupt
    PieCtrlRegs.PIEIER4.all  = 0;
    PieCtrlRegs.PIEIER5.all  = 0;
    PieCtrlRegs.PIEIER6.all  = 0;
    PieCtrlRegs.PIEIER7.all  = 0;
    PieCtrlRegs.PIEIER8.all  = 0;
    PieCtrlRegs.PIEIER9.all  = 0;
    PieCtrlRegs.PIEIER10.all = 0;
    PieCtrlRegs.PIEIER11.all = 0;
    PieCtrlRegs.PIEIER12.all = 0;

    // Disable CPU interrupts and clear all CPU interrupt flags:
    IER = 0x0000;
    IFR = 0x0000;

    // Initialize the PIE vector table
    InitPieVectTable();

    // Enable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;

    // Acknowledge all interrupts in PIE.
    // Enables PIE to drive a pulse into the CPU
    PieCtrlRegs.PIEACK.all = 0xFFFF;

    // Enable CPU INTx lines used in this project
    IER |= M_INT3; // INT3 which is connected to EPWM1-6 INT:

}

