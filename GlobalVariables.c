/*
 * GlobalVariables.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   Global Variables Declaration & Initialization.
//

#include "macros.h"            // Include macro definitions
#include "IQmathLib.h"         // Include IQmath functions & macro declarations
#include "GlobalVariables.h"   // Include Global Variable declarations

// Global variables used in this example

// Frequency and angles
_iq  freq, freq_set;
_iq  theta;
_iq  sinTheta, cosTheta;

// Voltages
_iq  Vdc;
_iq  Vd, Vq;
_iq  Valfa, Vbeta;
_iq  Va, Vb, Vc;

// PWM Timing computation
_iq  Ta, Tb, Tc;
_iq  Tmax, Tmin;
_iq  Teff, Tzero, Toffset;
_iq  Tao, Tbo, Tco;
//
// PWM technique
pwm_t pwm;

//---------------------------------------------------------------------------
// InitVars:
//---------------------------------------------------------------------------
// This function initializes the Global Variables to a known (default) state.
void InitVars(void)
{
    // Initialize System Variables
    freq_set = _IQ(F_RATED);
    Vdc      = _IQ(VDC);

    // Initialize State Variables
    freq  = 0;
    theta = 0;

    // Initialize PWM technique
    pwm = SINE;
}
